# config.fish: Main configuration file for fish shell
#
# Copyright (c) 2020 Trey Boehm
#
# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# There's a little bit of bootstrapping involved. I want FISH_CONF_DIR to be set in environment.sh, but I need to know
# where it is to source it. Hard-code the path.
source ~/.config/environment/environment.sh

# If pyenv is available we need to set it up before `set_path`.
if [ (which pyenv) ]
    status --is-interactive; and pyenv init - | source
end

# Set up the PATH
source $FISH_CONF_DIR/functions/set_path.fish
set_path

# Shorten each directory to 3 characters in the prompt pwd
set -gx fish_prompt_pwd_dir_length 3

if status --is-interactive
    # Use direnv, if available.
    which -s direnv && direnv hook fish | source

    # Set up zoxide, if available.
    which -s zoxide && zoxide init --cmd cd fish | source

    # Set up atuin, if available.
    if [ "$TERM" != "dumb" ]
        which -s atuin && atuin init fish | source
    end
end

# No greeting message each time you spawn a shell
set fish_greeting

# OS-specific options
switch (uname)
case Darwin
    source $FISH_CONF_DIR/functions/macos.fish
    set_macos
case Linux
    source $FISH_CONF_DIR/functions/linux.fish
    set_linux
end

set -l scripts \
    user_functions \
    local_functions \
    python_functions \
    work \

for f in $scripts
    set -l script_path "$FISH_CONF_DIR/functions/$f.fish"
    test -f "$script_path" && source "$script_path"
end
