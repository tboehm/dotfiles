function set_linux -d 'Set variables for fish on Linux'
    if string match -q "st-*" "$TERM"
        set -e VTE_VERSION
    end

    xdg-user-dirs-update --set DESKTOP $HOME
    xdg-user-dirs-update --set DOWNLOADS $HOME

    set -gx LFS   /mnt/lfs
    set -gx PINTOS $HOME/code/os-2
    set -gx RISCV /usr/local/bin/riscv
    set -gx XILINX /opt/Xilinx

    if ! [ "$TERM" = "dumb" ]
        set -gx TERM "screen-256color"
    end
end

#function alsamixer -d 'Force alsa to use card 0, not Pulse'
#    set -l param -c 0
#    command alsamixer $param $argv
#end

function aptdate -d 'Update, upgrade, autoremove/purge'
    sudo apt update
    sudo apt --yes dist-upgrade
    sudo apt --yes autoremove --purge
end

function dvorak -d 'Switch to Dvorak keyboard layout'
    setxkbmap dvorak
    xmodmap -e "clear lock" -e "keycode 0x42 = Escape"
end

function intl -d 'International Dvorak layout (diacritics)'
    setxkbmap us -variant dvorak-intl
    xmodmap -e "clear lock" -e "keycode 0x42 = Escape"
end

function ls -d 'list directory contents'
    command ls -F --color=auto $argv
end

function mountt -d 'Formatted mount output'
    command mount $argv \
        | sed -e 's/on\ //' \
        | sed -e 's/type\ //' \
        | column -t
end

# mpv with my USB DAC
alias mpvh "mpv --audio-device='alsa/default:CARD=Device'"

function ncm -d 'ncmpcpp paths specified'
    set -l param '--config=$HOME/.config/ncmpcpp/config'
    set -l param '--bindings=$HOME/.config/ncmpcpp/bindings'
    command ncmpcpp $param $argv
    rm -r $HOME/.ncmpcpp
    rm -r $HOME/.lyrics
end

function wicd-gtk -d 'Do not leave a hanging process after termination'
    set -l param --no-tray
    command wicd-gtk $param $argv
    rmdir $HOME/.wicd
end

function wtf -d 'WTF is going on with the kernel?'
    set -l dmesg_cmd "sudo dmesg --color=always | tail"
    set -l syslog_cmd "sudo tail /var/log/syslog"
    echo $dmesg_cmd
    eval $dmesg_cmd
    echo $syslog_cmd
    eval $syslog_cmd
end

function zathura -d 'A document viewer'
    set -l param --fork
    command zathura $param $argv
end
