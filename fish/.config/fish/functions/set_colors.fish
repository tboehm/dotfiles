function set_colors --description 'Set color variables'
    shell_color
    man_color
end

function shell_color --description 'Set shell color variables'
    # See here for a list of colors: https://fishshell.com/docs/current/interactive.html
    set -U fish_color_command magenta
    set -U fish_color_cwd magenta --bold
    set -U fish_color_cwd_root yellow --bold

    set -U fish_color_root red
    set -U fish_color_error red --bold
    set -U fish_color_escape blue # cyan
    set -U fish_color_history_current blue # cyan
    # set -U fish_color_host blue --bold # cyan --bold
    set -U fish_color_match blue # cyan
    set -U fish_color_normal normal
    set -U fish_color_operator blue # cyan
    set -U fish_color_param blue # cyan
    set -U fish_color_quote brown
    set -U fish_color_redirection magenta
    set -U fish_color_search_match --background=magenta
    # set -U fish_color_status red
    # set -U fish_color_user green --bold
    set -U fish_color_valid_path --underline
    set -U fish_pager_color_completion normal
    set -U fish_pager_color_selected_background yellow
    set -U fish_pager_color_selected_description normal
    set -U fish_pager_color_selected_completion normal
    set -U fish_pager_color_prefix blue # cyan
    set -U fish_pager_color_progress blue # cyan
    set -U fish_color_autosuggestion normal --dim
    set -U fish_color_comment normal --dim
    set -U fish_pager_color_description normal --dim
end

function man_color --description 'Set man page color variables'
    set -U LESS_TERMCAP_md (printf "\e[01;33m")
    set -U LESS_TERMCAP_me (printf "\e[0m")
    set -U LESS_TERMCAP_se (printf "\e[0m")
    set -U LESS_TERMCAP_so (printf "\e[01;44;39m")
    set -U LESS_TERMCAP_ue (printf "\e[0m")
    set -U LESS_TERMCAP_us (printf "\e[01;36m")
end
