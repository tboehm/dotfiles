function set_path --description '[Re]set the $PATH variable'
    fish_add_path --append --move \
        $PYENV_ROOT/shims \
        /opt/homebrew/opt/bison/bin \
        /bin \
        /sbin \
        /usr/bin \
        /usr/sbin \
        /usr/local/bin \
        /usr/local/sbin \
        /Library/TeX/texbin \
        /opt/homebrew/bin \
        $HOME/bin \
        $HOME/.local/bin \

end

function set_macos_path --description 'Set the system-wide path with launchctl'
    sudo launchctl config user path (echo $PATH | sed 's/ /:/g')
end
