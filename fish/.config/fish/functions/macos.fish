function set_macos --description 'Set variables for fish on macOS'
    # | Type                                             | Code | Foreground        | Background |
    # |--------------------------------------------------+------+-------------------+------------|
    # | directory                                        | ex   | blue              | default    |
    # | symbolic link                                    | fx   | magenta           | default    |
    # | socket                                           | cx   | green             | default    |
    # | pipe                                             | Gx   | bold cyan         | default    |
    # | executable                                       | bx   | red               | default    |
    # | block special                                    | Ca   | bold green        | black      |
    # | character special                                | Da   | bold yellow       | black      |
    # | executable with setuid bit set                   | HB   | bold bright white | bold red   |
    # | executable with setgid bit set                   | Ad   | bold dark grey    | yellow     |
    # | directory writable to others, with sticky bit    | ac   | black             | green      |
    # | directory writable to others, without sticky bit | he   | light grey        | blue       |
    set -gx LSCOLORS exfxcxGxbxCaDaHBAdache
    # Use https://geoff.greer.fm/lscolors/ to convert to LS_COLORS.
    set -gx LS_COLORS "di=34:ln=35:so=32:pi=1;36:ex=31:bd=1;32;40:cd=1;33;40:su=1;37;1;41:sg=1;30;43:tw=30;42:ow=37;44"
end

function ls --description 'list directory contents'
    command ls -FG $argv
end

alias rm 'trash'
abbr -a -g rmm 'command rm'
