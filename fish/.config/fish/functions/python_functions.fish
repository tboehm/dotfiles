function venv -d 'Python 3 virtualenv'
    command python3 -m venv $argv
end

function venv-activate -d 'Activate a virtualenv by name or path' -a venv_name
    if ! set -q argv[1]
        echo "Usage: venv-activate <venv_name>"
        return 1
    end

    set -l test_venv "$VIRTUAL_ENV_DIR/$venv_name"
    if ! [ -d "$test_venv" ]
        set -e test_venv
        echo "$venv_name is not a valid virtual environment"
        return 1
    end
    set -gx VIRTUAL_ENV "$test_venv"

    if test -n "$_OLD_VIRTUAL_PATH"
        # Another venv is active. Remove its bin path from $PATH.
        set -gx PATH "$_OLD_VIRTUAL_PATH"
    end
    set -gx _OLD_VIRTUAL_PATH "$PATH"
    set -gx PATH "$VIRTUAL_ENV/bin" $PATH
end

function venv-create -d 'Create a new virtualenv in the $VIRTUAL_ENV_DIR' -a venv_name
    if ! set -q argv[1]
        echo "Usage: venv-create <venv_name>"
        return 1
    end
    set -l venv_path "$VIRTUAL_ENV_DIR/$venv_name"
    if test -d "$venv_path"
        echo "virtualenv '$venv_name' already exists in '$VIRTUAL_ENV_DIR'"
        return 1
    end
    venv "$venv_path"
end

# List virtual environments
alias venv-list "env TERM=dumb ls -1 $VIRTUAL_ENV_DIR"

abbr -a -g p3 ipython

alias pip "pip --require-virtualenv"
