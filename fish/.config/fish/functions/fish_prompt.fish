function shorten_cwd --description "Shorten the current working directory to fit in the terminal"
    set min_chars_to_truncate 6
    set truncate_chars 3
    set -l max_cols (echo (tput cols) - 36 | bc)

    # TODO: Always show the current directory
    # TODO: Color the parent dirs differently

    set -l me (whoami)
    set -l cwd (pwd | sed "s,/Users/$me,~,")
    set -l new_len (string length $cwd)
    set -l new_cwd
    for dir in (string split '/' $cwd)
        if [ -z $dir ]
            continue
        else if [ $new_len -lt $max_cols -o (string length $dir) -le $min_chars_to_truncate ]
            set new_cwd (string join '/' $new_cwd $dir)
        else
            set -l first_chars (echo $dir | cut -c -$truncate_chars)
            set new_cwd (string join '/' $new_cwd $first_chars)
            set new_len (math $new_len - (string length $dir) + $truncate_chars)
        end
    end

    # Repeat the process with a lower truncate_chars (copy/pasted for now)
    if [ $new_len -gt $max_cols ]
        set cwd $new_cwd
        set new_cwd
        for dir in (string split '/' $cwd)
            if [ -z $dir ]
                continue
            else if [ $new_len -lt $max_cols ]
                set new_cwd (string join '/' $new_cwd $dir)
            else
                set -l first_chars (echo $dir | cut -c 1)
                set new_cwd (string join '/' $new_cwd $first_chars)
                set new_len (math $new_len - (string length $dir) + 1)
            end
        end
    end

    if [ -z $new_cwd ]
        set new_cwd "/"
    else if ! [ (echo $new_cwd | cut -c 1) = "~" ]
        set new_cwd '/'$new_cwd
    end

    printf '%s' "$new_cwd"
end

set -gx _last_time (date +%s)

function fish_prompt --description "Write out the prompt"
    set -l last_status $status

    if [ "$TERM" = dumb ]
        echo "\$ "
        return
    end

    # Options for a more verbose and colorful git prompt
    set -gx __fish_git_prompt_show_informative_status 1
    set -gx __fish_git_prompt_showdirtystate 1
    set -gx __fish_git_prompt_showuntrackedfiles 1
    set -gx __fish_git_prompt_showcolorhints 1

    set -l normal (set_color normal)

    # Hack; fish_config only copies the fish_prompt function (see #736)
    if not set -q -g __fish_classic_git_functions_defined
        set -g __fish_classic_git_functions_defined

        function __fish_repaint_user --on-variable fish_color_user --description "Event handler, repaint when fish_color_user changes"
            if status --is-interactive
                commandline -f repaint ^/dev/null
            end
        end

        function __fish_repaint_host --on-variable fish_color_host --description "Event handler, repaint when fish_color_host changes"
            if status --is-interactive
                commandline -f repaint ^/dev/null
            end
        end

        function __fish_repaint_status --on-variable fish_color_status --description "Event handler; repaint when fish_color_status changes"
            if status --is-interactive
                commandline -f repaint ^/dev/null
            end
        end

        function __fish_repaint_bind_mode --on-variable fish_key_bindings --description "Event handler; repaint when fish_key_bindings changes"
            if status --is-interactive
                commandline -f repaint ^/dev/null
            end
        end
    end

    set -l prefix
    set -l fp_mode
    set -l fp_user
    set -l fp_host
    set -l fp_cwd
    set -l fp_vcs
    set -l fp_status
    set -l suffix

    #################### MODE ####################
    # "erase" the existing mode function so I can replace it with my own
    functions --erase fish_mode_prompt

    switch $fish_bind_mode
        case default
            set fp_mode (set_color -o -b blue black)'[N]'(set_color -b normal)' '
        case insert
            set fp_mode (set_color -b normal)''
        case visual
            set fp_mode (set_color -o -b yellow black)'[V]'(set_color -b normal)' '
        case replace_one
            set fp_mode (set_color -o -b red black)'[R]'(set_color -b normal)' '
    end

    #################### USER ####################
    if [ $USER = root ]
        set fp_user (set_color -r red) "!" $normal
        set suffix (set_color -o $fish_color_root) " #"
    else
        set fp_user ""
        # set fp_user (set_color $fish_color_user) $USER " " $normal
        set suffix (set_color -o red) " >" (set_color -o green) ">" (set_color -o blue) ">"
    end

    #################### HOSTNAME ####################
    if [ "$SSH_CONNECTION" = "" ]
        set fp_host ""
    else
        set fp_host $normal@(set_color $fish_color_host)(prompt_hostname)$normal
    end

    #################### CWD ####################
    if [ $USER = root ]
        # set fp_cwd (set_color $fish_color_cwd_root) (prompt_pwd) $normal
        # set fp_cwd (set_color $fish_color_cwd_root)(pwd)$normal
        set fp_cwd (set_color $fish_color_cwd_root)(shorten_cwd)$normal
    else
        # set fp_cwd (set_color $fish_color_cwd) (prompt_pwd) $normal
        # set fp_cwd (set_color $fish_color_cwd)(pwd)$normal
        set fp_cwd (set_color $fish_color_cwd)(shorten_cwd)$normal
    end


    #################### VIRTUAL ENVIRONMENT ####################
    # Make sure that the current virtual environment matches the expected in a given tree.
    # If not, indicate to the user with a red, bold warning.

    set venv_name (string split "/" "$VIRTUAL_ENV" | tail -1)
    if [ "$venv_name" != "" ]
        set fp_venv (set_color 4B8BBE)"($venv_name)"(set_color -o normal)" "
    else
        set fp_venv ""
    end

    #################### VCS ####################
    # set fp_vcs (set_color $fish_color_vcs) (fish_vcs_prompt) $normal
    # set fp_vcs (fish_vcs_prompt) $normal

    if [ $FISH_NO_VCS_PROMPT ]
        set fp_vcs ""
    else
        if set -l git_branch (command git symbolic-ref HEAD 2>/dev/null | string replace refs/heads/ '')
            set git_branch (set_color --italic --dim)"$git_branch"
            set -l git_status
            # if not command git diff-index --quiet HEAD --
                if set -l count (command git rev-list --count --left-right $upstream...HEAD 2>/dev/null)
                    echo $count | read -l ahead behind
                    if test "$ahead" -gt 0
                        set git_status "$git_status"(set_color -o white)⬆
                    end
                    if test "$behind" -gt 0
                        set git_status "$git_status"(set_color -o white)⬇
                    end
                end
                for i in (git status --porcelain | string sub -l 2 | sort | uniq)
                    switch $i
                        case "."
                            set git_status "$git_status"(set_color green)✚
                        case " D"
                            set git_status "$git_status"(set_color red)✖
                        case "*M*"
                            # set git_status "$git_status"(set_color green)✱
                            set git_status "$git_status"(set_color yellow)Δ
                        case "*R*"
                            set git_status "$git_status"(set_color purple)➜
                        case "*U*"
                            set git_status "$git_status"(set_color brown)═
                        case "??"
                            set git_status "$git_status"(set_color white)≠
                    end
                end
            # end
            if [ "$git_status" = "" ]
                set git_status (set_color green)"✔"
            end
            set fp_vcs "($git_branch"(set_color normal)":$git_status"(set_color normal)") "
        end
    end

    #################### STATUS ####################
    if [ $last_status -ne 0 ]
        set fp_status " " (set_color $fish_color_status) "[$last_status]" "$normal"
    else
        set fp_status "$normal"
    end

    #################### TIME ####################
    set elapsed_seconds (expr $CMD_DURATION / 1000)
    if [ $elapsed_seconds -eq 0 ]
        set fp_time "["(date +"%H:%M:%S")"] "
    else
        set fp_time "["(date +"%H:%M:%S") "/" (timedelta $elapsed_seconds)"] "
    end

    printf '%b' "\n$fp_venv$fp_time$fp_vcs$fp_mode$fp_user$fp_host$fp_cwd\n$fp_status$normal~> "
end

