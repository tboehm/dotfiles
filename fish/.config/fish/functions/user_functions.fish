function !! --description 'Run the previous command'
    set var (history | head -n 1)
    if [ $argv != "" ]
        if [ $argv = "sudo" ]
            eval $argv $var
        else
            eval $var $argv
        end
    else
        eval $var
    end
end

# Typing 'ls' is uncomfortable in Dvorak
abbr -a -g a ls

function base16 --description 'Reload the color scheme'
    set -l theme_file $HOME/.config/base16-theme
    set -l base16_scripts $HOME/.config/base16-shell/scripts
    if test -f $theme_file
        set theme (head -n1 $theme_file)
        sh $base16_scripts/$theme.sh
    end
end

function base16-dark --description 'Change the color scheme to base16-gruvbox-dark-hard'
    printf '%b' 'base16-gruvbox-dark-hard\n' > $HOME/.config/base16-theme
    base16
end

function base16-light --description 'Change the color scheme to base16-gruvbox-light-soft'
    printf '%b' 'base16-gruvbox-light-soft\n' > $HOME/.config/base16-theme
    base16
end

alias bat 'bat --theme=ansi --italic-text=always'
abbr -a -g cat bat

function bc --description 'An arbitrary precision calculator language'
    command bc -l
end

function brewup -d 'Update brew and upgrade all formulae and casks'
    brew update
    brew upgrade
    brew upgrade --cask
end

function cabal --description 'Command line interface to the Haskell Cabal infrastructure'
    command cabal --config-file=$HOME/.config/cabal/config $argv
end

function cal3 --description 'Calendar with 2 additional months'
    cal -A 2 $argv
end

function cdl --description 'cd into the latest directory'
    set -l last_dir (ls -tr -d */ | tail -1)
    if [ $last_dir != "" ]
        cd $last_dir
    end
end

function cleandir --description 'Clean a directory files that LaTeX and lilypond may generate'
    for f in {*.aux,*.dvi,*.fls,*.fdb_latexmk,*.log,*.out,*.midi,*.synctex.gz,*.toc}
        command rm -v $f
    end
end

function connected --description 'Show IPs of connected devices'
    netstat -tn | grep :80 | awk '{print $5}' | cut -d: -f1 | sort | uniq -c | sort -n
end

# Breakdown of storage usage, 1 level deep
abbr -a -g du1 'du -h -d1'

# Breakdown of storage usage, 1 level deep, sorted
abbr -a -g du1s 'du -h -d1 | sort -h'

# Use Emacs in non-window (terminal) mode
abbr -a -g emacs 'emacs -nw'
abbr -a -g em 'emacs -nw'

# Edit a file in Emacs without creating a new frame (no "-c") and or waiting for the user to kill the buffer ("-n").
alias e 'emacsclient -n -a ""'

# Edit a file in Emacs in non-windowed mode ("-nw"), creating a new frame ("-c").
alias et 'emacsclient -nw -c -a ""'

# Edit ~/.config/fish/config.fish
abbr -a -g fishrc vim $HOME/.config/fish/config.fish

function fish_user_key_bindings
    for mode in default insert visual
        fish_default_key_bindings -M $mode
    end
    fish_vi_key_bindings --no-erase
end

function fedit --description 'Edit a fish function in its original file'
    # TODO: Completions
    functions $argv \
        # The first line is "# Defined in /path/to/function @ line N"
        | head -n1 \
        # Print "/path/to/function +N"
        | awk '{print $4,"+"$7}' \
        # Edit at that line number (vim or nvim)
        | xargs $EDITOR
end

alias fzf "fzf --preview='$FZF_PREVIEW_COMMAND'"

function gdbb --description "Use gdb in fish by setting the shell to bash"
    command env SHELL=/bin/bash gdb $argv
end

# The Glasgow Haskell Compiler with my defaults
alias ghc "ghc -l param -hidir hs-build-files -odir hs-build-files --make -o hs.out"

# The Glasgow Haskell Compiler without .ghc and ghci_history
alias ghci "ghci -ignore-dot-ghci -fno-ghci-history"

# Save a couple keypresses and forgive typos
abbr -a -g g git
abbr -a -g gti git

# Use ~/.config/gnupg/ as the default gpg dir
alias gpg "gpg --homedir $HOME/.config/gnupg"

# hledger convenience
abbr -a -g hl hledger

# Use Vi keybindings for reading GNU info documents
alias info "info --vi-keys"

# Use neovim, if present
which -s nvim && alias vim nvim

# Vim and some of my common mis-typings of it
abbr -a -g ivm vim
abbr -a -g vmi vim

# List all contents of a directory
alias la "ls -lAFh $argv"

# ls without color
alias lnc "ls --color=none"

# Long, all, human-readable, time
alias lt "ls -lAht --color=always"

# lynx web browser with vi keybindings
alias lynx "lynx -vikeys"

# Make parent dirs if they do not exist
alias mkdir "mkdir -p"

# mutt with proper config file
alias mutt "mutt -F $HOME/.config/mutt/mutt.rc"

# Edit ~/.config/nvim/init.vim
abbr -a -g vimrc $EDITOR $HOME/.config/nvim/init.vim

# More useful (to me) mpv status message
alias mpv "mpv --term-status-msg='\${time-pos} / \${duration} (\${percent-pos}%) - \${audio-bitrate}'"

# Test the internet connection. Tries to open a connection to Google's DNS.
alias p "nc -vz -w 2 8.8.8.8 53"

function please --description 'Run the previous command with sudo'
    !! sudo
end

# Stream from Progzilla Radio with mpv
alias progzilla "mpv http://stream1.hippynet.co.uk:8005:/live"

function psgrep --description 'grep for a process'
    ps -e -o ruser,pid,stat,%cpu,%mem,vsz,rss,time,start,command \
        | grep -v grep \
        | grep -v "ps -ejvr" \
        | grep $argv -i --color=auto
end

function python3 --description 'get rid of the pesky .python_history file'
    command python3 $argv
    command rm -f $HOME/.python_history
end

function reload --description 'Reload config.fish, set path, and set colors'
    source $HOME/.config/fish/config.fish
    set_path
    set_colors
    base16
end

function rmempty --description 'Remove empty directories in the current working directory'
    set directories $argv
    if [ "$directories" = "" ]
        set directories (pwd)
    end

    for dirname in $directories
        while fd . "$dirname" --type empty --quiet
            find "$dirname/" -type d -empty -exec rm -r {} \+
        end
    end
end

# Use smart case with ripgrep
alias rg 'rg -S'

function sorted --description 'Sort the lines of a file'
    sort $argv > $argv.sorted
    mv $argv.sorted $argv
end

# Better default output settings
alias timidity "timidity --output-mono --output-24bit"

# Make sure direnv has not set anything up when first starting a session
# Use configuration file ~/.config/tmux/tmux.conf
alias tmux "direnv exec / tmux -f $HOME/.config/tmux/tmux.conf"

# Edit the tmux configuration file
abbr -a -g tmuxrc $EDITOR $HOME/.config/tmux/tmux.conf

# List tmux panes with their names, PIDs, pane IDs, and indices
alias panes 'tmux list-panes -a -F "#{session_name}:#{window_index}:#{pane_index} [PID #{pane_pid} / pane ID #{pane_id}] -- #{pane_title}"'

# Open the Todo page in vimwiki
alias todo "vim -c \"VimwikiIndex\""

# All files, color, human-readable, ignore the .git directory
alias tree "tree -aCh -I .git"

# Length of files in the current tree
alias treesize "fd --type file | xargs wc -l | sort --numeric-sort"

function timedelta --description "Format a time delta as HH:MM:SS"
    if [ "$argv" = "" ]
        echo ""
    end
    set total $argv
    set hours (expr $total / 3600)
    set total (expr $total - 3600 \* $hours)
    set minutes (expr $total / 60)
    set seconds (expr $total - 60 \* $minutes)
    if [ "$hours" != "0" ]
        echo $hours:(string pad -c 0 -w 2 $minutes):(string pad -c 0 -w 2 $seconds)
    else if [ "$minutes" != "0" ]
        echo $minutes:(string pad -c 0 -w 2 $seconds)
    else
        echo $seconds sec
    end
end

function underspace --description 'Replace underscores with spaces in filenames'
    for file in *
        mv $file (echo "$file" | sed -e 's/_/\ /g')
    end
end

function uniquify --description 'Get rid of duplicate lines in a file'
    sorted $argv
    uniq $argv > $argv.uniq
    mv $argv.uniq $argv
end

function vi --description 'Vim with custom init'
    command vim -u $HOME/.config/vim/vimrc $argv
end

function zeropad --description 'Prepend a 0 to all filenames in a dir'
    for file in *
        mv $file (echo 0"$file")
    end
end

# Invoke the editor on a file chosen from fzf
function zn --description 'Invoke the editor on a file chosen from fzf'
    $EDITOR (fzf)
end
