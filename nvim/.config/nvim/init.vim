" init.vim
" Maintainer: Trey Boehm

" ====> Look and feel <=================================================
let os = substitute(system('uname'), "\n", "", "") " OS-specific changes

let mapleader = " " " space bar for the leader

" colorscheme slate
set cursorline      " highlight the current line
set relativenumber  " number lines relative to the current line
set number          " number the current line (hybrid mode)
augroup numtoggle   " hybrid numbering only in focused buffer and
                    "     while in normal/visual mode
    autocmd!
    autocmd BufEnter,FocusGained,InsertLeave * set relativenumber
    autocmd BufLeave,FocusLost,InsertEnter   * set norelativenumber
augroup end
set showcmd         " show (partial) command in status line
set wildmenu        " visually show suggested commands w/ tab
set showmatch       " show matching brackets

" Statusline
set noshowmode      " do not show mode in cmdline

" Highlight all trailing whitespace
autocmd InsertLeave * match Error /\s\+$/
" In insert mode, ignore the current line's trailing whitespace
autocmd InsertEnter * match Error /\s\+\%#\@<!$/

" Italic comments
hi Comment cterm=italic gui=italic

" Backspace anything in insert mode
set backspace=indent,eol,start

" Always show at least 1 lines at the top/bottom of the screen
set scrolloff=1

" Always include 2 columns (for ALE) to the left of the numbers
" set signcolumn:yes:1

" Defaults for running :make. Use /bin/sh for minimal fluff in output.
set shell=/bin/sh
nnoremap <Leader>m :make<CR>

" :W should just save
command! W :w
command! Wq :wq
command! Wqa :wqa
command! Q :q
command! Qa :qa

" Highlight the last inserted text
nnoremap gV `[v`]

" Use the system clipboard ('+' register) by default
" For visual block paste, use ""p or ""P
set clipboard=unnamedplus

" Write to the swap file after 100 ms of no activity
set updatetime=100

" Do not insert comment leader on previous/next line when using o/O
set formatoptions-=o

" Only add one space after '.', '?', and '!' with a join command
set nojoinspaces

function! ToggleAutoFormat()
    if stridx(&formatoptions, 'a') == -1
        set formatoptions+=a
        echo 'Enabled auto-format'
    else
        set formatoptions-=a
        echo 'Disabled auto-format'
    endif
endfunction
inoremap <C-q> <C-\><C-o>:call ToggleAutoFormat()<CR>

" ====> Indentation and folding <=======================================
set tabstop=8       " most editors have tab configured to be 8 spaces
set softtabstop=4   " a tab counts as 4 spaces when editing
set shiftwidth=4    " how many columns are indented using << and >>
set expandtab       " pressing tab creates some number of spaces
filetype plugin indent on " Use filetype-specific indentation rules
set autoindent      " indent hitting enter in an indentation block
set smartindent     " knows when to stop indentation

" Don't unindent # character (Python comments in a block, mainly)
inoremap # X#

set foldenable
set foldlevelstart=4    " all folds level 4 and lower are shown
set foldnestmax=4       " no more than 4 folds
set foldmethod=indent   " indentation blocks are folds
set fillchars="\ "      " chars after a closed fold are blank

" ====> Searching and navigation <======================================
" if has("nvim")
"     lua require("navigation")
" endif

set ignorecase      " do case insensitive matching
set smartcase       " do smart case matching
set incsearch       " show matches as characters are entered
set hlsearch        " highlight matches
hi Search ctermfg=white ctermbg=DarkBlue

" Turn off search highlight
nnoremap <leader>hh :nohlsearch<CR>

" Incremental substitution
set inccommand=nosplit

" Don't recreate the previous line when mouse scrolling in insert mode
set mouse=a

" Mouse pointer is hidden when characters are typed (automatic on macOS)
set mousehide

" Load .local.vim from the base directory of a git repo. Do this before
" looking at project-specific variables (g:project_root).
let local_vimrc = system("printf $(git rev-parse --show-toplevel)/.local.vim")
if filereadable(local_vimrc)
    let g:project_root = fnamemodify(local_vimrc, ':p:h')
    exec "source " . local_vimrc
endif

" Set g:project_root to the top level of a project in a local vimrc to
" allow for better ctags/cscope management.
if !exists('g:project_root')
    let g:project_root = expand('%:p:h')
endif

" Update the path for keybindings like gf
let &path = &path . ',' . g:project_root

if !exists('g:ctags_flags')
    let g:ctags_flags = ''
endif

let g:tags_file = fnameescape(g:project_root) . '/tags'
let g:cscope_db = fnameescape(g:project_root) . '/cscope.out'

" Generate tags file (depends on TagHighlight plugin) and cscope db
function! UpdateProjectTags()
    execute 'cd ' . g:project_root
    execute '!ctags -R ' . g:ctags_flags . ' -f tags'
    UpdateTypesFileOnly
    !cscope -b -R
    cd -
    echo 'Project tags and cscope database updated'
endfunction

nnoremap <Leader>t :call UpdateProjectTags()<CR>

" " Use <Leader>] to jump to a tag's definition in a new tab
nmap <silent><Leader>] <C-w><C-]><C-w>T

" When switching buffers (e.g. tag jump), consider windows in other tab pages
set switchbuf=usetab

" Load cscope database
function! LoadCscopeDB()
    if filereadable(g:cscope_db)
        execute 'silent cs add ' . g:cscope_db . ' ' . g:project_root
    elseif $CSCOPE_DB != ""
        silent cs add $CSCOPE_DB
    endif
endfunction

" cscope settings
if has("cscope")
    " au BufEnter /* call LoadCscopeDB()

    " set cscopetag     " Use both cscope and ctags
    " set csto=1        " Use ctags first, then cscope
    " set csto=0          " Use cscope first, then ctags
    "set cscopeverbose " Message when cscope adds a db
    " TODO: Make a function to:
    "   1) Save cwd
    "   2) cd to g:project_root
    "   3) Perform the cscope action
    "   4) cd back to the saved cwd

    " Keybindings are all some control key combination plus another key:
    " 's' - symbol:   find all references to the token under the cursor
    " 'g' - global:   find global definitions of the token
    " 'c' - calls:    find all calls to the function
    " 't' - text:     find all instances of text
    " 'e' - egrep:    egrep search for the word
    " 'f' - file:     open the filename under the cursor
    " 'i' - includes: find files that include the filename under cursor
    " 'd' - called:   find functions that the function calls
    " Ctrl-s to show results in the current window (Ctrl-T to go back)
    " Ctrl-\ to split window and show results there
    nmap <C-\>s :scs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>g :scs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>c :scs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>t :scs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>f :scs find f <C-R>=expand("<cword>")<CR><CR>
    nmap <C-\>i :scs find i ^<C-R>=expand("<cword>")<CR>$<CR>
    nmap <C-\>d :scs find d <C-R>=expand("<cword>")<CR><CR>

    nmap <C-s>s :cs find s <C-R>=expand("<cword>")<CR><CR>
    nmap <C-s>g :cs find g <C-R>=expand("<cword>")<CR><CR>
    nmap <C-s>c :cs find c <C-R>=expand("<cword>")<CR><CR>
    nmap <C-s>t :cs find t <C-R>=expand("<cword>")<CR><CR>
    nmap <C-s>f :cs find f <C-R>=expand("<cword>")<CR><CR>
    nmap <C-s>i :cs find i ^<C-R>=expand("<cword>")<CR>$<CR>
    nmap <C-s>d :cs find d <C-R>=expand("<cword>")<CR><CR>
endif

" ====> Macros <========================================================

" Disable highlight search (for macro use)
let @n = ":set nohls"

" Enable highlight search, but turn off matches immediately (for macro use)
let @l = ":set hls:noh"

" Visually select a function in a C-like language
" 1) Disable highlight search
" 2) Search for the pattern from https://stackoverflow.com/questions/2109503/
" 3) Go to the blank line above the function
" 4) Go down a line to get the cursor on the start of the prototype
" 5) Enter visual line mode
" 6) Find the next open curly brace
" 7) Go to the matching curly brace
" 8) Turn of search highlighting
" 9) Enable highlight search
let @f = "@n?^[^ \t#]{jV/{%"

" Add inline Doxygen-style comments for each argument to a function
" 1) Disable highlight search
" 2) Visually select the paragraph
" 3) Replace all linstances of "," with ", ///<"
" 4) Go down one line (the last argument doesn't have a trailing comma)
" 5) Append " ///<"
" 6) Invoke easy-align on the paragraph to align the comments
" 7) Enable highlight search
let @m = "@nvip:s/,/,\ \\\/\\\/\\\/</jA ///<gaip///<"

" Format a C function prototype from the source code for a header file
" 1) Disable highlight search
" 2) Visually select the paragraph
" 3) Join
" 4) Go to the first ( on the line
" 5) Get the first argument on its own line an unindent once
" 6) Replace all instances of ",\s\+" on this line with ",\r    "
" 7) Append a ; to the line
" 8) Go left one character and get the ); on its own line
" 9) Deindent
" 10) Add Doxygen-style comments for each argument
" 11) Enable highlight search
let @h = "@nvipJ0f(a:.s/,\\s\\+/,\\r\ \ \ \ /geA;hi@m"

" Remove inline argument comments from a C function prototype
" 1) Disable highlight search
" 2) Visually select the paragraph
" 3) Remove all trailing comments
" 4) Visually select the paragraph
" 5) Remove all trailing whitespace
" 6) Enable highlight search
let @d = "@nvip:s/\\\/\\+.*$//evip:s/\\s\\+$//e"

" Format a C function declaration from the header file for source code
" 1) Remove any inline comments (@d)
" 2) Go to the start of the paragraph
" 3) Find the ( that starts the argument list
" 4) Insert a newline before the function name
" 5) Visually select to the end of the paragraph
" 6) Join
" 7) Delete the ; at the end of the line
" 8) Go to the matching parenthesis
" 9) Delete the extra space character
" 10) Apply formatprg to the line
" 11) Add curly braces on subsequent lines
" 12) Enter insert mode in the body of the new function
let @c = "@d{wf(bhclv}J$x%lxgqqo{}O"

" Join a paragraph
" 1) Visually select the paragraph
" 2) Join
" 3) Go to the start of the line
" 4) Go down two lines (to get to the next paragraph)
let @j = "vipJ^jj"

" ====> Miscellaneous <=================================================
" Files withn #! and /bin/ on the first line are executable
function ModeChange()
    if getline(1) =~ "^#!"
        if getline(1) =~ "/bin/"
            silent !chmod +x <afile>
        endif
    endif
endfunction

au BufWritePost * call ModeChange()

" Clear trailing whitespace from files
command! RMW :%s/\s\+$//e <Bar> :nohlsearch

" Jump to last location when opening a file
au BufReadPost * if line("'\"") > 1
            \ && line("'\"") <= line("$")
            \ | exe "normal! g'\""
            \ | endif

" Reverse lines in a file, convert commas (+whitespace) to tabs, and strip
" leading whitespace
command! RevTab :g/^/m0<Bar> :%s/,\s*/\t/g<Bar> :%s/^\s\+//g<Bar>

" Edit binary files with xxd
nmap <Leader>hr :%!xxd<CR> :set filetype=xxd<CR>
nmap <Leader>hw :%!xxd -r<CR> :set binary<CR> :set filetype=<CR>

" TODO: Use pyenv
" let g:python3_host_prog = '/usr/local/bin/python3'

" ====> Plugins <=======================================================
" If re-installing:
" 0) cd ~/etc/
" 1) clone github.com/junegunn/vim-plug
" 2) ln -s ~/etc/vim-plug/plug.vim ~/.config/nvim/autoload/plug.vim
" 3) :PlugUpdate
call plug#begin('$HOME/.config/nvim/plugged')

Plug 'https://gitlab.com/tboehm/riscv-syntax.git/', " RISC-V syntax :)

Plug 'airblade/vim-gitgutter',          " Git diff in the gutter
Plug 'ARM9/arm-syntax-vim',             " Syntax for ARM assembly
Plug 'cespare/vim-toml',                " Syntax for TOML
Plug 'dag/vim-fish',                    " fish scripts
Plug 'dense-analysis/ale',              " Asynchronous Lint Engine
Plug 'gisraptor/vim-lilypond-integrator', " Lilypond
Plug 'honza/vim-snippets',              " Actual snippets
" Plug 'itchyny/lightline.vim',           " Statusline/tabeline
Plug 'junegunn/limelight.vim',          " Hyperfocus-writing in Vim
Plug '/usr/local/opt/fzf',              " Fuzzy finder
Plug 'junegunn/fzf.vim',                " Fuzzy finder
Plug 'junegunn/vim-easy-align',         " Alignment
Plug 'justinmk/vim-sneak',              " 2-character f/t
Plug 'kshenoy/vim-signature',           " Help with marks
Plug 'ledger/vim-ledger',               " ledger/hledger
Plug 'lervag/vimtex',                   " LaTeX
Plug 'majutsushi/tagbar',               " Tag interface
Plug 'mechatroner/rainbow_csv',         " Highlight and query CSVs
Plug 'miikanissi/modus-themes.nvim',    " Modus Themes from Emacs (by Protesilaos)
Plug 'neovimhaskell/haskell-vim',       " Haskell syntax
Plug 'octol/vim-cpp-enhanced-highlight', " Better syntax for C++
Plug 'psf/black', { 'tag': '19.10b0' }, " Python code formatter
Plug 'rktjmp/git-info.vim',             " Git info for status line
Plug 'Shougo/deoplete.nvim',            " Autocompletion framework
Plug 'shumphrey/fugitive-gitlab.vim',   " GitLab support for Fugitive
Plug 'tpope/vim-commentary',            " Comment help
Plug 'tpope/vim-fugitive',              " Git wrapper
Plug 'tpope/vim-repeat',                " Enable . for supported plugins
Plug 'tpope/vim-rsi',                   " Readline style insertion
Plug 'tpope/vim-surround',              " Quotes, parentheses, etc.
Plug 'tpope/vim-unimpaired',            " Pairs of handy bracket mappings
Plug 'vim-python/python-syntax',        " Better syntax for Python
Plug 'vim-scripts/TagHighlight',        " Highlighting based on tag
Plug 'vimwiki/vimwiki',                 " Personal Wiki for vim
Plug 'vhda/verilog_systemverilog.vim',  " Verilog syntax
Plug 'zchee/deoplete-jedi',             " Python

call plug#end()

" ====> Plugin options <================================================
" ------------> airblade/vim-gitgutter <--------------------------------
let g:gitgutter_preview_win_floating = 0
nmap <Leader>GT <Plug>(GitGutterToggle)

" ------------> dag/vim-fish <------------------------------------------
" Support for editing fish scripts
autocmd FileType fish compiler fish " :make use fish for syntax checking

" ------------> dense-analysis/ale <------------------------------------
nmap <Leader>n :ALENext<CR>
nmap <Leader>p :ALEPrevious<CR>
nmap <Leader>d :ALEDetail<CR>

hi link clear ALEError
hi link clear ALEErrorSign
hi link clear ALEErrorSignLineNr
hi link clear ALEWarning
hi link clear ALEWarningSign
hi link clear ALEWarningSignLineNr

hi ALEError             ctermfg=black ctermbg=red cterm=underline
hi ALEErrorSign         ctermfg=red
hi ALEErrorSignLineNr   ctermfg=red
hi ALEWarning           cterm=underline
hi ALEWarningSign       ctermfg=yellow
hi ALEWarningSignLineNr ctermfg=yellow

let g:ale_sign_highlight_linenrs = 1

" ------------> itchyny/lightline.vim <---------------------------------
let g:lightline = {
        \ 'active': {
        \   'left': [ [ 'mode', 'paste' ],
        \             [ 'fugitive', 'readonly',
        \               'filename', 'modified' ]],
        \   'right': [ [ 'percent', 'lineinfo' ],
        \              [ 'fileformat', 'fileencoding', 'filetype' ]]
        \ },
        \ 'component': {
        \   'lineinfo': '%3l:%-2v',
        \ },
        \ 'component_function': {
        \   'fugitive': 'LightlineFugitive',
        \   'readonly': 'LightlineReadonly'
        \ },
        \ 'separator': { 'left': '', 'right': '' },
        \ 'subseparator': { 'left': '|', 'right': '|' },
        \ 'tabline': {
        \   'left': [ [ 'tabs' ] ],
        \   'right': [ [ '' ] ]
        \ },
        \ 'tabline_separator': { 'left': '', 'right': '|' },
        \ 'tabline_subseparator': { 'left': '', 'right': '' }
        \ }

let g:lightline.inactive = copy(g:lightline.active)

function! LightlineReadonly()
    return &readonly ? '[RO]' : ''
endfunction

function! LightlineFugitive()
    if exists('*fugitive#head')
        let branch = fugitive#head()
        return branch
    endif
    return ''
endfunction

" ------------> junegunn/fzf <------------------------------------------
nmap <C-p> :FZF<CR>
nmap <Leader>fz :FZF<CR>

" No line numbers in FZF window
autocmd! FileType fzf
autocmd  FileType fzf setlocal nonu nornu

" Default extra key bindings
let g:fzf_action = {
  \ 'ctrl-t': 'tab split',
  \ 'ctrl-x': 'split',
  \ 'ctrl-w': 'split',
  \ 'ctrl-v': 'vsplit' }

" Customize fzf colors to match your color scheme
let g:fzf_colors =
\ { 'fg':      ['fg', 'Normal'],
  \ 'bg':      ['bg', 'ColorColumn'],
  \ 'hl':      ['fg', 'SpecialChar'],
  \ 'fg+':     ['fg', 'String'],
  \ 'bg+':     ['bg', 'Normal'],
  \ 'hl+':     ['fg', 'Statement'],
  \ 'info':    ['fg', 'Type'],
  \ 'border':  ['fg', 'Normal'],
  \ 'prompt':  ['fg', 'Conditional'],
  \ 'pointer': ['fg', 'Exception'],
  \ 'marker':  ['fg', 'Keyword'],
  \ 'spinner': ['fg', 'Label'],
  \ 'header':  ['fg', 'Comment'] }

let fzf_window_position = {'down': '60%'}
let fzf_preview_position = 'up:60%'
let fzf_rg_cmd = 'rg --column --line-number --no-heading --color=always --smart-case'
let fzf_gg_cmd = 'git grep --line-number'

" Ripgrep from the project root
command! -bang -nargs=* Rg
    \ call fzf#vim#grep(
    \   fzf_rg_cmd.' '.shellescape(<q-args>),
    \   1,
    \   fzf#vim#with_preview({'down': '60%',
    \                         'options': ['--layout=reverse', '--info=inline'],
    \                         'dir': g:project_root},
    \                         'up:60%'),
    \   <bang>0)
nnoremap <Leader>fr :Rg<CR>
nnoremap <Leader>fR :Rg!<CR>

" Git grep
" TODO: Fail gracefully if `git rev-parse` fails
command! -bang -nargs=* GGrep
    \ call fzf#vim#grep(
    \   fzf_gg_cmd.' '.shellescape(<q-args>),
    \   0,
    \   fzf#vim#with_preview({'down': '60%',
    \                         'options': ['--layout=reverse', '--info=inline'],
    \                         'dir': systemlist('git rev-parse --show-toplevel')[0]},
    \                         'up:60%'),
    \   <bang>0)
nnoremap <Leader>fg :GGrep<CR>
nnoremap <Leader>fG :GGrep!<CR>

" Files with previews
command! -bang -nargs=? -complete=dir Files
    \ call fzf#vim#files(
    \   <q-args>,
    \   fzf#vim#with_preview({'down': '60%',
    \                         'options': ['--layout=reverse', '--info=inline']},
    \                         'up:60%'),
    \   <bang>0)
nnoremap <Leader>fz :Files<CR>
nnoremap <Leader>fZ :Files!<CR>

" Git files with previews
command! -bang -nargs=? -complete=dir GFiles
    \ call fzf#vim#gitfiles(
    \   <q-args>,
    \   fzf#vim#with_preview({'down': '60%',
    \                         'options': ['--layout=reverse', '--info=inline']},
    \                         'up:60%'),
    \   <bang>0)
nnoremap <Leader>ff :GFiles<CR>
nnoremap <Leader>fF :GFiles!<CR>

" Lines in the current buffer
nnoremap <Leader>fl :BLines<CR>
nnoremap <Leader>fL :BLines!<CR>

" Lines in all loaded buffers
nnoremap <Leader>fa :Lines<CR>
nnoremap <Leader>fA :Lines!<CR>

" Enable per-command history.
let g:fzf_history_dir = '$HOME/.local/share/fzf-history'

" ------------> junegunn/limelight.vim <--------------------------------
nmap <Leader>l <Plug>(Limelight)
xmap <Leader>l <Plug>(Limelight)
nmap <Leader>L :Limelight!<CR>

" ------------> junegunn/vim-easy-align <-------------------------------
" Start interactive EasyAlign in visual mode (e.g. vipga)
xmap ga <Plug>(EasyAlign)
" Start interactive EasyAlign for a motion/text object (e.g. gaip)
nmap ga <Plug>(EasyAlign)

" ------------> lervag/vimtex <-----------------------------------------
" Fix the issue where vimtex keeps a background process
let g:vimtex_compiler_latexmk = {'backend' : 'process'}
let g:vimtex_compiler_progname = 'nvr'
" Spellchecking in LaTeX files (z= over a word gives suggestions)
autocmd FileType tex setlocal spell spelllang=en_us spellsuggest=best,5
" Default to LaTeX
let g:tex_flavor = 'latex'

" ------------> majutsushi/tagbar <-------------------------------------
nnoremap <Leader>b :TagbarToggle<CR>

" ------------> miikanissi/modus-themes.nvim <--------------------------

lua << EOF
require("modus-themes").setup({
    style = "modus_vivendi",
    variant = "tinted",
    styles = {
        functions = { italic = true }, -- Enable italics for functions
    },
})
EOF
colorscheme modus

" ------------> psf/black <---------------------------------------------
let g:black_skip_string_normalization = 1
let g:black_linelength = 100

" ------------> Shougo/deoplete.nvim <----------------------------------
" <TAB> to autocomplete a deoplete suggestion

function! s:check_back_space() abort
    let col = col('.') - 1
    return !col || getline('.')[col - 1]  =~ '\s'
endfunction

inoremap <silent><expr> <TAB>
            \ pumvisible() ? "\<C-n>" :
            \ <SID>check_back_space() ? "\<TAB>" :
            \ deoplete#manual_complete()

" Disable deoplete by default and enable on a per-filetype basis
let g:deoplete#enable_at_startup = 0

" Don't open the preview window
set completeopt-=preview

" ------------> tpope/vim-fugitive <------------------------------------
nmap <Leader>gs :Gstatus<CR>
nmap <Leader>gw :Gwrite<CR>
nmap <Leader>gr :Gread<CR>
nmap <Leader>gb :Gblame<CR>
nmap <Leader>gd :Gdiffsplit<CR>

" ------------> vim-scripts/TagHighlight <------------------------------
if ! exists('g:TagHighlightSettings')
    let g:TagHighlightSettings = {}
endif
let g:TagHighlightSettings['TagFileName'] = 'tags'
if os == "Linux"
    let g:TagHighlightSettings['CtagsExecutable'] = '/usr/bin/ctags'
else
    let g:TagHighlightSettings['CtagsExecutable'] = '/usr/local/bin/ctags'
endif

" Entries in TagHighlight/plugin/TagHighlight/data/kinds.txt
hi CTagsDefinedName      cterm=bold ctermfg=red   ctermbg=none
hi CTagsEnumerationValue cterm=bold ctermfg=white ctermbg=none
hi CTagsNamespace        cterm=bold ctermfg=blue  ctermbg=none

hi link CTagsGlobalVariable Special
hi link CTagsExtern         Special
hi link CTagsGlobalVariable Special
hi link CTagsMember         None

" ------------> vim-python/python-syntax <------------------------------
let g:python_highlight_all = 1

" ------------> vim-wiki/vim-wiki <-------------------------------------
let wiki = {}
let wiki.path = '$HOME/Documents/vimwiki'
let wiki.nested_syntaxes = {
            \ 'asm': 'asm',
            \ 'bash': 'bash',
            \ 'c': 'c',
            \ 'c++': 'cpp',
            \ 'python': 'python',
            \ 'rust': 'rust',
            \ 'sv': 'systemverilog',
            \ }
let g:vimwiki_list = [wiki]
let g:vimwiki_folding = 'expr'

nnoremap glt :VimwikiToggleListItem<CR>

" ------------> zchee/deoplete-jedi <-----------------------------------
let g:jedi#use_tabs_not_buffers = 1
