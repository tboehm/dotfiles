call deoplete#enable()

" --aosp: 4-space indentation
" -: Operate on stdin
let &equalprg = "google-java-format --aosp -"

set textwidth=100
