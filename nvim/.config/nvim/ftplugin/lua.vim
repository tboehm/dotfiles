call deoplete#enable()

" Use lua-format for equalprg
if executable("lua-format")
    let &equalprg="lua-format"
    let lua_format_yaml=expand("$HOME/.config/lua/lua-format.yaml")
    if filereadable(lua_format_yaml)
        let &equalprg=&equalprg . ' -c ' . lua_format_yaml
    endif
    unlet lua_format_yaml
endif
