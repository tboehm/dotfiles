set textwidth=80

" tagbar acts like a table of contents
let g:tagbar_type_vimwiki = {
          \   'ctagstype':'vimwiki'
          \ , 'kinds':['h:header']
          \ , 'sro':'&&&'
          \ , 'kind2scope':{'h':'header'}
          \ , 'sort':0
          \ , 'ctagsbin':'$HOME/etc/vimwiki-utils/vwtags.py'
          \ , 'ctagsargs': 'default'
          \ }
