call deoplete#enable()
setlocal makeprg=python3\ %

if !exists('g:ale_python_flake8_options')
    let g:ale_python_flake8_options =
                \ '--config=' . expand('$HOME/.config/flake8')
                \ . ' --ignore=E126,E228,E265,E302,E305,E501,E722'
endif

let b:ale_linters = ['flake8']
