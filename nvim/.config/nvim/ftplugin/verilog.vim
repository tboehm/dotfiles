call deoplete#enable()

let b:ale_linters = ['verilator']

" tpope/vim-commentary
setlocal commentstring=//\ %s
