set commentstring=\%\ %s
set shiftwidth=2

let &makeprg = "docker run -v $(pwd):/ly codello/lilypond %"
