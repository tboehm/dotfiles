function! FoldLevel(lnum)
    let l:fold1_regex = '^\"\ ====>.*$'
    let l:fold2_regex = '^\"\ ------------>.*$'
    if (getline(a:lnum)) =~ l:fold1_regex
        return '>1'
    elseif (getline(a:lnum + 1)) =~ l:fold1_regex
        return '<1'
    elseif (getline(a:lnum)) =~ l:fold2_regex
        return '>2'
    elseif (getline(a:lnum + 1)) =~ l:fold2_regex
        return '<2'
    else
        return '='
    endif
endfunction

set foldenable
set foldmethod=expr
set foldexpr=FoldLevel(v:lnum)
set foldlevel=0
