call deoplete#enable()

" Match shorthand LaTeX parentheses (\lp and \rp)
if exists("g:tex_no_error") && g:tex_no_error
    let s:tex_no_error= 1
else
    let s:tex_no_error= 0
endif
if !s:tex_no_error
    syn region texMatcher matchgroup=Delimiter start="lp" end="rp" transparent contains=@texMatchGroup,texError,@NoSpell
else
    syn region texMatcher matchgroup=Delimiter start="lp" end="rp" transparent contains=@texMatchGroup,
endif
if !s:tex_no_error
 syn match texError "[}\])]\\rp]"
endif

let g:vimtex_quickfix_ignore_filters = [
      \ 'Overfull \\hbox',
      \ 'Underfull \\hbox',
      \ ]

" j and k move by visual line (through wrapped text)
nnoremap j gj
nnoremap k gk
