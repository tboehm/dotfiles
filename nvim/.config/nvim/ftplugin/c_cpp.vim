call deoplete#enable()

" Special highlighting for Doxygen comments
let g:load_doxygen_syntax = 1

hi def link doxygenComment                SpecialComment
hi def link doxygenBrief                  Comment
hi def link doxygenBody                   Comment
hi def link doxygenSpecialTypeOnelineDesc Comment
hi def link doxygenBOther                 Comment
hi def link doxygenParam                  Comment
hi def link doxygenParamName              Comment
hi def link doxygenSpecialOnelineDesc     Comment
hi def link doxygenSpecialHeading         Statement
hi def link doxygenPrev                   SpecialComment

" Use uncrustify for equalprg
if executable("uncrustify")
    let &equalprg="uncrustify -q"
    if &ft == "cpp"
        let &equalprg = &equalprg . " -l cpp"
    else
        let &equalprg = &equalprg . " -l c"
    endif

    let uncrustify_cfg = expand("$UNCRUSTIFY_CONFIG")
    if filereadable(uncrustify_cfg)
        let &equalprg = &equalprg . " -c " . uncrustify_cfg
    endif
    unlet uncrustify_cfg
endif

" Set header filetype to 'C' if a corresponding C file exists.
" FIXME: This function doesn't behave as expected. Maybe put in ftdetect?
function! SwitchHeaderType()
    " path/to/file.h -> path/to/file.c
    let c_file = expand("%:p:r") . ".c"
    " path/to/inc/file.h -> path/to/src/file.c
    let c_src_file = substitute(c_file, "inc", "src", "")
    if or(filereadable(c_file), filereadable(c_src_file))
        setlocal filetype=c
    endif
endfunction
"autocmd BufReadPost * call SwitchHeaderType()

" tpope/vim-commentary
setlocal commentstring=//\ %s

if &ft == "cpp"
    let g:ale_linters["cpp"] = ["g++"]
else
    let g:ale_linters["c"] = ["gcc"]
endif
