let s:black          = [ g:base16_gui00,  0 ]
let s:red            = [ g:base16_gui08,  1 ]
let s:green          = [ g:base16_gui0B,  2 ]
let s:yellow         = [ g:base16_gui0A,  3 ]
let s:blue           = [ g:base16_gui0D,  4 ]
let s:magenta        = [ g:base16_gui0E,  5 ]
let s:cyan           = [ g:base16_gui0C,  6 ]
let s:white          = [ g:base16_gui05,  7 ]
let s:bright_black   = [ g:base16_gui03,  8 ]
let s:bright_red     = [ g:base16_gui08,  9 ]
let s:bright_green   = [ g:base16_gui0B, 10 ]
let s:bright_yellow  = [ g:base16_gui0A, 11 ]
let s:bright_blue    = [ g:base16_gui0D, 12 ]
let s:bright_magenta = [ g:base16_gui0E, 13 ]
let s:bright_cyan    = [ g:base16_gui0C, 14 ]
let s:bright_white   = [ g:base16_gui07, 15 ]
let s:bright_orange  = [ g:base16_gui09, 16 ]
let s:dark_orange    = [ g:base16_gui0F, 17 ]
let s:dark_grey      = [ g:base16_gui01, 18 ]
let s:middle_grey    = [ g:base16_gui02, 19 ]
let s:light_grey     = [ g:base16_gui04, 20 ]
let s:middle_white   = [ g:base16_gui06, 21 ]

let s:p = {'normal': {}, 'inactive': {}, 'insert': {}, 'replace': {}, 'visual': {}, 'tabline': {}}

let s:p.normal.left     = [ [ s:black, s:blue, 'bold' ],    [ s:blue,    s:black ] ]
let s:p.normal.middle   = [ [ s:white, s:dark_grey ] ]
let s:p.normal.right    = [ [ s:black, s:blue ],            [ s:white,   s:middle_grey ] ]
let s:p.insert.left     = [ [ s:black, s:green, 'bold' ],   [ s:green,   s:black ] ]
let s:p.replace.left    = [ [ s:black, s:red, 'bold' ],     [ s:red,     s:black ] ]
let s:p.visual.left     = [ [ s:black, s:yellow, 'bold' ],  [ s:yellow, s:black ] ]

let s:p.inactive.left   = [ [ s:light_grey, s:dark_grey ],    [ s:light_grey,   s:dark_grey ] ]
let s:p.inactive.middle = [ [ s:dark_grey, s:dark_grey ] ]
let s:p.inactive.right  = copy(s:p.inactive.left)

let s:p.tabline.left    = [ [ s:middle_grey, s:black, 'underline' ] ]
let s:p.tabline.tabsel  = [ [ s:bright_white, s:dark_grey, 'bold' ] ]
let s:p.tabline.middle  = copy(s:p.tabline.left)
let s:p.tabline.right   = copy(s:p.tabline.left)

let s:p.normal.error    = [ [ s:black, s:red ] ]
let s:p.normal.warning  = [ [ s:black, s:yellow ] ]

let g:lightline#colorscheme#base16#palette = lightline#colorscheme#flatten(s:p)
