#
# modified version of uncrustify config file for the linux kernel
# https://github.com/uncrustify/uncrustify/blob/master/etc/linux.cfg
#

indent_with_tabs    = 0     # spaces only
input_tab_size      = 8     # original tab size
output_tab_size     = 4     # new tab size
indent_columns      = 4     # number of columns to indent per level
indent_align_string = true  # indent strings broken by '\' to align
indent_brace        = 0
indent_label        = 1     # pos: absolute col, neg: relative column

#
# inter-symbol newlines
#

nl_enum_brace        = remove    # "enum {" vs "enum \n {"
nl_union_brace       = remove    # "union {" vs "union \n {"
nl_struct_brace      = remove    # "struct {" vs "struct \n {"
nl_do_brace          = remove    # "do {" vs "do \n {"
nl_if_brace          = remove    # "if () {" vs "if () \n {"
nl_for_brace         = remove    # "for () {" vs "for () \n {"
nl_else_brace        = remove    # "else {" vs "else \n {"
nl_while_brace       = remove    # "while () {" vs "while () \n {"
nl_switch_brace      = remove    # "switch () {" vs "switch () \n {"
nl_brace_while       = remove    # "} while" vs "} \n while" - cuddle while
nl_brace_else        = remove    # "} else" vs "} \n else" - cuddle else
sp_brace_else        = force
sp_else_brace        = force
nl_func_var_def_blk  = 1
nl_fcall_brace       = remove    # "list_for_each() {" vs "list_for_each()\n{"
nl_fdef_brace        = add       # "int foo() {" vs "int foo()\n{"
# nl_after_label_colon = true      # "fail:\nfree(foo);" vs "fail: free(foo);"


#
# Source code modifications
#

mod_paren_on_return  = remove # "return 1;" vs "return (1);"
mod_full_brace_if    = force  # "if (a) { a--; }" vs "if (a) a--;"
#mod_full_brace_if_chain = true
mod_full_brace_for   = force  # "for () { a--; }" vs "for () a--;"
mod_full_brace_do    = force  # "do { a--; } while ();" vs "do a--; while ();"
mod_full_brace_while = force  # "while (a) { a--; }" vs "while (a) a--;"
mod_full_brace_nl    = 3      # don't remove if more than 3 newlines
mod_sort_include     = true   # works if not order-dependent

include_category_0   = "<.*>$"
include_category_1   = "\".*\"$"


#
# inter-character spacing options
#

sp_return_paren          = force  # "return (1);" vs "return(1);"
sp_sizeof_paren          = remove # "sizeof (int)" vs "sizeof(int)"
sp_do_brace_open         = force  # "do {" vs "do{"
sp_brace_close_while     = force  # "} while" vs "}while"
sp_while_paren_open      = force  # "while (" vs "while("
sp_before_sparen         = force  # "if (" vs "if("
sp_after_sparen          = force  # "if () {" vs "if (){"
sp_after_cast            = remove # "(int) a" vs "(int)a"
sp_inside_braces         = force  # "{ 1 }" vs "{1}"
sp_inside_braces_struct  = force  # "{ 1 }" vs "{1}"
sp_inside_braces_enum    = force  # "{ 1 }" vs "{1}"
sp_assign                = force
sp_arith                 = force
sp_bool                  = force
sp_compare               = force
sp_assign                = force
sp_after_comma           = force
sp_func_def_paren        = remove # "int foo (){" vs "int foo(){"
sp_func_call_paren       = remove # "foo (" vs "foo("
sp_func_proto_paren      = remove # "int foo ();" vs "int foo();"
sp_before_tr_emb_cmt     = force  # space before trailing comment
sp_num_before_tr_emb_cmt = 1      # must be at least one space


#
# Line splitting options
#

code_width         = 100 # try to limit code width to N columns
ls_for_split_full  = false # split long 'for' statements at semi-colons
ls_func_split_full = true # split long func prototypes/calls at commas
ls_code_width      = false # don't override the two above options


#
# Aligning stuff
#

align_with_tabs            = false # don't use tabs to align
align_on_tabstop           = false # don't force align on tabstops
align_keep_tabs            = false
# align_enum_equ_span        = 4     # '=' in enum definition
indent_cmt_with_tabs       = false
align_right_cmt_span       = 2     # align comments +/- 2 lines
align_right_cmt_gap        = 1     # one space before comments
align_right_cmt_same_level = true  # only align at the same brace level
align_right_cmt_at_col     = 1     # pull aligned comments in to the left
align_func_params          = true
align_struct_init_span     = 1     # align struct initialization
align_single_line_brace    = false
align_var_struct_span      = 1     # align struct definition
align_var_def_span         = 1     # align variable declarations/definitions
# The next two options don't work as expected
align_var_def_amp_style    = 2     # dangling star before name
align_var_def_star_style   = 2     # dangling star before name
align_pp_define_span       = 2
align_pp_define_gap        = 1
align_nl_cont              = true  # align macros wrapped with backslash

#
# C++
#

nl_class_colon = remove
nl_constr_colon = force
indent_class = true
sp_after_class_colon = add
sp_before_class_colon = add
indent_constr_colon = true
nl_collapse_empty_body = true
indent_func_call_param = true
indent_func_def_param = true
indent_func_proto_param = true
indent_func_class_param = true
indent_func_ctor_var_param = true
indent_template_param = true
indent_paren_after_func_call = false
indent_paren_close = 2

# Add or remove newline between C++11 lambda signature and '{'.
nl_cpp_ldef_brace               = remove   # ignore/add/remove/force

# Don't split one-line C++11 lambdas, as in '[]() { return 0; }'.
nl_cpp_lambda_leave_one_liners  = true     # true/false

# Add or remove newline between '}' and ')' in a function invocation.
# This makes lambdas and domain-specific constructs like parallel_for look nicer.
nl_brace_fparen                 = remove   # ignore/add/remove/force

# Add or remove space around '=' in C++11 lambda capture specifications.
sp_cpp_lambda_assign            = remove   # ignore/add/remove/force

# Add or remove space around assignment operator '=' in a prototype.
sp_assign_default               = remove   # ignore/add/remove/force

# The next two options don't quite work as I'd like. Be careful.
#cmt_width                 = 72    # try to wrap comments at N columns
#cmt_reflow_mode           = 2     # fully reflow comments
cmt_convert_tab_to_spaces = true  # no tabs at all
cmt_indent_multi          = true  # apply changes to multiline comments
cmt_c_nl_start            = false # /* on its own line
cmt_c_nl_end              = false # */ on its own line
cmt_star_cont             = true  # put * on subsequent lines
cmt_sp_before_star_cont   = 0     # 1 space before each *
cmt_sp_after_star_cont    = 0     # 1 space after each *
cmt_multi_check_last      = false

# Declarations: Make it easy to do Doxygen
nl_func_paren         = remove
nl_func_decl_start    = force
nl_func_decl_empty    = remove
nl_func_decl_args     = force
nl_func_decl_end      = force

# Definitions: Make it compact
nl_func_def_start           = remove
nl_func_def_start_single    = remove
nl_func_def_empty           = remove
nl_func_def_args            = remove
nl_func_def_args_multi_line = false
nl_func_def_end             = remove
sp_fparen_brace             = force

# Calls: Keep it on one line, unless it's too long. In that case, put every
# argument on its own line and the closing parenthesis on its own line, too.
nl_func_call_paren            = remove  # newline after func name/before paren?
nl_func_call_start_multi_line = true    # true/false
nl_func_call_args_multi_line  = true    # true/false
nl_func_call_end_multi_line   = true    # true/false

nl_func_type_name     = force  # newline between return type/func name
nl_after_func_body    = 2
nl_after_func_proto   = 1
nl_after_func_proto_group   = 2
nl_before_func_body_def = 2
sp_inside_paren       = remove
sp_inside_square      = remove
sp_inside_paren_cast  = remove
sp_inside_fparen      = remove
sp_inside_sparen      = remove
sp_paren_paren        = remove
sp_before_ptr_star    = force
sp_after_ptr_star     = remove
sp_between_ptr_star   = remove

eat_blanks_after_open_brace   = true
eat_blanks_before_close_brace = true

nl_squeeze_ifdef           = true   # remove nl around preproc
nl_squeeze_ifdef_top_level = true   # remove nl around top level preproc
pp_indent                  = remove # no preproc indentation

nl_start_of_file    = remove
nl_end_of_file      = force
nl_end_of_file_min  = 1
nl_comment_func_def = 1

# Add or remove space between '>' and '>' in '>>' (template stuff C++/C# only). Default=Add
sp_angle_shift = remove # ignore/add/remove/force

# Permit removal of the space between '>>' in 'foo<bar<int> >' (C++11 only). Default=False
# sp_angle_shift cannot remove the space without this option.
sp_permit_cpp11_shift = true # true/false

sp_template_angle               = force  # "template <typename T>" vs "template<typename T>"
sp_before_angle                 = remove
sp_inside_angle                 = remove
sp_inside_angle_empty           = remove
sp_angle_colon                  = remove
sp_after_angle                  = remove
sp_angle_paren                  = remove # "List<byte> (foo)" vs "List<byte>(foo)"
sp_angle_paren_empty            = remove
sp_angle_word                   = force  # "List<byte> m" vs "List<byte>m"
