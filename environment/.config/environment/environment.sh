# environment.sh: Set up environment variables and make sure their corresponding directories exist.
#
# This script aims to be compatible with both fish and POSIX shells:
#  - No local variables. Use `export` if necessary.
#  - No `if` statements. Use `&&`.
#
# Additionally, the only side effects of this script should be:
#  - Setting the values of environment variables.
#  - Creating directories.

# Try to wrangle config and cache files
export XDG_CONFIG_HOME="$HOME/.config"
export XDG_STATE_HOME="$HOME/.cache"
export XDG_DATA_HOME="$HOME/.local/share"

mkdir -p "$XDG_CONFIG_HOME"
mkdir -p "$XDG_STATE_HOME"
mkdir -p "$XDG_DATA_HOME"

# Top configuration directory for fish
export FISH_CONF_DIR="$XDG_CONFIG_HOME/fish"

# Help pdf-tools
# shellcheck disable=SC2155
export zlib_dir="$(find /opt/homebrew/Cellar/zlib -type d -depth 1 | head -1)"
test -d "$zlib_dir" && export PKG_CONFIG_PATH="$zlib_dir:/opt/homebrew/lib/pkgconfig"

# pyenv location
which -s pyenv && export PYENV_ROOT="$HOME/.pyenv"

# Configuration directory for ipython
export IPYTHONDIR="$XDG_CONFIG_HOME/ipython"

# Directory to hold virtualenvs
export VIRTUAL_ENV_DIR="$HOME/.local/virtualenvs"
mkdir -p "$VIRTUAL_ENV_DIR"

# Use ipdb to debug Python
export PYTHONBREAKPOINT="ipdb.set_trace"

# aspell config files
export ASPELL_CONF="home-dir $XDG_CONFIG_HOME/aspell/"

# gforth history file
export GFORTHHIST="$XDG_CONFIG_HOME/gforth/gforth-history"
# shellcheck disable=SC2086
mkdir -p "$(dirname $GFORTHHIST)"

# KiCad paths
export KICAD_DOCUMENTS_HOME="$HOME/Documents/kicad"
export KICAD_USER_TEMPLATE_DIR="$KICAD_DOCUMENTS_HOME/6.0/template/"	
export KICAD6_3RD_PARTY="$KICAD_DOCUMENTS_HOME/6.0/3rdparty"	

# Journal file for hledger
export LEDGER_FILE="$HOME/Documents/finance/journals/hledger.journal"

# less options: Quit-if-one-screen, ANSI color, no termcap, no history file
export LESS=FRX
export LESSHISTFILE="$XDG_STATE_HOME/less/history"

# Edit LilyPond links using emacsclient so we reuse the existing frame.
export LYEDITOR="emacsclient %(file)s +%(line)s:%(column)s"

# tmsu database
export TMSU_DB="$HOME/.local/tmsu/defaultdb"

# Terminal info
test "$TERM" != "dumb" && export TERMINFO="$XDG_CONFIG_HOME/terminfo"
test "$TERM" != "dumb" && export TERM=xterm-256color

# Configuration directory for uncrustify
export UNCRUSTIFY_CONFIG="$XDG_CONFIG_HOME/uncrustify/uncrustify.cfg"

# Use a different configuration directory for GPG
export GNUPGHOME="$XDG_CONFIG_HOME/gnupg"

# ZSH configuration files
which -s zsh && export ZDOTDIR="$XDG_CONFIG_HOME/zsh"

# Use bat as the preview for fzf
which -s fzf && export FZF_PREVIEW_COMMAND="bat --style=numbers --color=always --pager=never --theme=base16 {}"

# Figure out $EDITOR and friends.
export VIM_PROGNAME=vim
which -s nvim && export VIM_PROGNAME=nvim

# Use neovim for the manpager, if available
test "$VIM_PROGNAME" = nvim && export MANPAGER='nvim +Man!'

# zoxide database location
which -s zoxide && export _ZO_DATA_DIR="$XDG_DATA_HOME/zoxide"

export EDITOR=$VIM_PROGNAME
which -s emacsclient && export EDITOR="emacsclient -a $VIM_PROGNAME"
export SUDO_EDITOR="$EDITOR"
export GIT_EDITOR="$EDITOR"
export ALTERNATE_EDITOR="$VIM_PROGNAME"
