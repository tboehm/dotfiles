dotfiles
========

A few of my most important configuration files.

Usage
-----

The easiest way to set up is using [GNU Stow](https://www.gnu.org/software/stow/manual/stow.html). For example, to
create symlinks for the `fish` configuration files:

```sh
stow -t "$HOME" fish
```
